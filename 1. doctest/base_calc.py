class BasicCalc:

    def add(self, a, b):
        '''
        Add value a to value b.

        >>> calc = BasicCalc()
        >>> calc.add(1, 2)
        3

        May also add other types supported by Python:

        >>> calc.add('Hello', 'World')
        'HelloWorld'
        '''
        return a + b

    def sub(self, a, b):
        '''
        Substract value b from value a.

        >>> b = BasicCalc()
        >>> b.sub(7, 1)
        6
        '''
        return a - b

    def multiply(self, a, b):
        '''
        Multiply values a and b.

        >>> b = BasicCalc()
        >>> b.multiply(2, 2)
        4

        May also be used to multiply strings like this:

        >>> b.multiply('!', 5)
        '!!!!!'
        '''
        return a * b

    def divide(self, a, b):
        '''
        Divide a by b.

        >>> b = BasicCalc()
        >>> b.divide(6, 2)
        3.0
        '''
        return a / b
