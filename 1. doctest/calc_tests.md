# Calculator module

Are you tired of typing mathematic operations directly into your Python console? These days are gone! Now you can do the same in a more complicated way!

Just import the BasicCalc module:

>>> from base_calc import BasicCalc

Instantiate it:

>>> calc = BasicCalc()

And let yourself loose:

>>> calc.add(1, 2)
3
>>> calc.sub(4, 2)
2
>>> calc.multiply(7, 8)
56
>>> calc.divide(1000, 10)
100.0

