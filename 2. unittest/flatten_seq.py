
def flatten_seq(seq) -> list:
    """
    Convert sequence seq of embedded sequences into a plain list.
    Supported sequence types: dict, list, tuple.

    >>> seq = ['chap1.md', {'introduction': 'chap2.md'}, ['chap3.md', 'chap4.md']]
    >>> flatten_seq(seq)
    ['chap1.md', 'chap2.md', 'chap3.md', 'chap4.md']

    If seq is not a supported sequence - returns list with just seq value.

    >>> flatten_seq('String')
    ['String']
    """
    result = []
    if type(seq) == dict:
        vals = seq.values()
    elif type(seq) in (list, tuple):
        vals = list(seq)
    else:
        vals = [seq]

    for i in vals:
        if type(i) in (dict, list, tuple):
            result.extend(flatten_seq(i))
        else:
            result.append(i)
    return result
