from unittest import TestCase
from flatten_seq import flatten_seq


class TestFlattenSeq(TestCase):
    def test_flatten_list(self):
        list1 = ['ch1.md', 'ch2.md', 'ch3.md']
        expected1 = list1
        list2 = ['ch1.md']
        expected2 = list2
        self.assertEqual(flatten_seq(list1), expected1)
        self.assertEqual(flatten_seq(list2), expected2)

    def test_flatten_embedded_lists(self):
        list1 = ['ch1.md', ['ch2.md', 'ch3.md']]
        expected1 = ['ch1.md', 'ch2.md', 'ch3.md']
        list2 = ['ch1.md', ['ch2.md', ['ch3.md', 'ch4.md']], 'ch5.md']
        expected2 = ['ch1.md', 'ch2.md', 'ch3.md', 'ch4.md', 'ch5.md']
        list3 = [['ch1.md', 'ch2.md'],
                 ['ch3.md', 'ch4.md'],
                 ['ch5.md', 'ch6.md']]
        expected3 = ['ch1.md', 'ch2.md',
                     'ch3.md', 'ch4.md',
                     'ch5.md', 'ch6.md']

        self.assertEqual(flatten_seq(list1), expected1)
        self.assertEqual(flatten_seq(list2), expected2)
        self.assertEqual(flatten_seq(list3), expected3)

    def test_flatten_dict(self):
        dict1 = {'Monolith': 'ch1.md'}
        expected1 = ['ch1.md']
        dict2 = {'Introduction': 'ch1.md',
                 'Main Part': 'ch2.md',
                 'Summary': 'ch3.md'}
        expected2 = ['ch1.md', 'ch2.md', 'ch3.md']
        self.assertEqual(flatten_seq(dict1), expected1)
        self.assertEqual(flatten_seq(dict2), expected2)

    def test_flatten_embedded_dicts(self):
        dict1 = {'Introduction': 'ch1.md',
                 'Main Part': {'Volume 1': 'ch2.md',
                               'Volume 2': 'ch3.md'},
                 'Summary': 'ch4.md'}
        expected1 = ['ch1.md', 'ch2.md', 'ch3.md', 'ch4.md']
        dict2 = {'Introduction': {'Volume 1': 'ch1.md',
                                  'Volume 2': 'ch2.md'},
                 'Main Part': {'Volume 1': 'ch3.md',
                               'Volume 2': 'ch4.md'},
                 'Summary': {'Volume 1': 'ch5.md',
                             'Volume 2': 'ch6.md'}}
        expected2 = ['ch1.md', 'ch2.md', 'ch3.md',
                     'ch4.md', 'ch5.md', 'ch6.md']
        dict3 = {'Introduction': {'Volume 1': {'Part1': {'first_half': 'ch1.md',
                                                         'second_half': 'ch2.md'},
                                               'Part2': {'first_half': 'ch3.md',
                                                         'second_half': 'ch4.md'}},
                                  'Volume 2': 'ch5.md'},
                 'Main Part': 'ch6.md',
                 'Summary': 'ch7.md'}
        expected3 = ['ch1.md', 'ch2.md', 'ch3.md',
                     'ch4.md', 'ch5.md', 'ch6.md',
                     'ch7.md']
        self.assertEqual(flatten_seq(dict1), expected1)
        self.assertEqual(flatten_seq(dict2), expected2)
        self.assertEqual(flatten_seq(dict3), expected3)

    def test_flatten_embedded_dicts_and_lists(self):
        seq1 = ['ch1.md', {'Main Part': 'ch2.md'}]
        expected1 = ['ch1.md', 'ch2.md']
        seq2 = ['ch1.md', {'Main Part': ['ch2.md', 'ch3.md']}, 'ch4.md']
        expected2 = ['ch1.md', 'ch2.md',
                     'ch3.md', 'ch4.md']
        seq3 = ['ch1.md',
                [{'Main': 'ch2.md',
                  'Not Main': 'ch3.md'},
                 'ch4.md'],
                'ch5.md']
        expected3 = ['ch1.md', 'ch2.md',
                     'ch3.md', 'ch4.md',
                     'ch5.md']
        seq4 = {'Introduction': ['ch1.md', 'ch2.md'],
                'Main': ['ch3.md', 'ch4.md']}
        expected4 = ['ch1.md', 'ch2.md',
                     'ch3.md', 'ch4.md']
        self.assertEqual(flatten_seq(seq1), expected1)
        self.assertEqual(flatten_seq(seq2), expected2)
        self.assertEqual(flatten_seq(seq3), expected3)
        self.assertEqual(flatten_seq(seq4), expected4)

    def test_flatten_not_a_sequence(self):
        val1 = 'String'
        expected1 = ['String']
        val2 = 5
        expected2 = [5]
        val3 = None
        expected3 = [None]
        self.assertEqual(flatten_seq(val1), expected1)
        self.assertEqual(flatten_seq(val2), expected2)
        self.assertEqual(flatten_seq(val3), expected3)
